tabbed - generic tabbed interface
=================================
tabbed is a simple tabbed X window container.

Requirements
------------
In order to build tabbed you need the Xlib header files.

(OPTIONAL)SauceCodePro Nerd Font https://github.com/ryanoasis/nerd-fonts

Installation
------------
Edit config.mk to match your local setup (tabbed is installed into
the /usr/local namespace by default).

Remove config.h

    rm -f config.h

Afterwards enter the following command to build and install tabbed
(if necessary as root):

    make clean install

Running tabbed
--------------
See the man page for details.


HOTKEYS
--------------
Spawn new window:
- Alt + Return

Go to tab:
- Alt + (1-9-0)

Change tabs with arrow keys:
- Alt + Left|Right Arrow

Move tab:
- Alt + j	(left)
- Alt + k	(right)

Go to previous tab:
- Alt + Tab

Close Tab:
- Alt + w

Toggle fullscreen:
- F11

